package conversor;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ConversorTest {
	
	Conversor conversor;
	
	@Before
	public void setUp() throws Exception {
		conversor =  new Conversor();
	}
	
	@Test
	public void testCelsiusParaFahrenheit() {
		assertEquals(104, conversor.celsiusParaFahrenheit());
	}
	@Test
	public void testCelsiusParaKelvin() {
		assertEquals(313, conversor.celsiusParaKelvin());
	}
	@Test
	public void testFahrenheitParaCelsius() {
		assertEquals(4, conversor.fahrenheitParaCelsius());
	}
	@Test
	public void testFahrenheitParaKelvin() {
		assertEquals(277, conversor.fahrenheitParaKelvin());
	}
	@Test
	public void testKelvinParaCelsius() {
		assertEquals(-233, conversor.kelvinParaCelsius());
	}
	@Test
	public void testKelvinParaFahrenheit() {
		assertEquals(-387, conversor.kelvinParaFahrenheit());
	}
	
	@Test
	public void testIntCelsiusParaFahrenheit() {
		assertEquals(104, conversor.celsiusParaFahrenheit(40));
	}
	@Test
	public void testIntCelsiusParaKelvin() {
		assertEquals(313, conversor.celsiusParaKelvin(40));
	}
	@Test
	public void testIntFahrenheitParaCelsius() {
		assertEquals(4, conversor.fahrenheitParaCelsius(40));
	}
	@Test
	public void testIntFahrenheitParaKelvin() {
		assertEquals(277, conversor.fahrenheitParaKelvin(40));
	}
	@Test
	public void testIntKelvinParaCelsius() {
		assertEquals(-233, conversor.kelvinParaCelsius(40));
	}
	@Test
	public void testIntKelvinParaFahrenheit() {
		assertEquals(-387, conversor.kelvinParaFahrenheit(40));
	}
	
	@Test
	public void testDoubleCelsiusParaFahrenheit() {
		assertEquals(104, conversor.celsiusParaFahrenheit(40.00),0.001);
	}
	@Test
	public void testDoubleCelsiusParaKelvin() {
		assertEquals(313, conversor.celsiusParaKelvin(40.00),0.001);
	}
	@Test
	public void testDoubleFahrenheitParaCelsius() {
		assertEquals(4.44444, conversor.fahrenheitParaCelsius(40.00),0.0001);
	}
	@Test
	public void testDoubleFahrenheitParaKelvin() {
		assertEquals(277.444444, conversor.fahrenheitParaKelvin(40.00),0.0001);
	}
	@Test
	public void testDoubleKelvinParaCelsius() {
		assertEquals(-233, conversor.kelvinParaCelsius(40.00),0.0001);
	}
	@Test
	public void testDoubleKelvinParaFahrenheit() {
		assertEquals(-387.4, conversor.kelvinParaFahrenheit(40.00),0.0001);
	}

}
