package conversor;

public class Conversor {

	public int celsiusParaFahrenheit() {
		return ((9 * 40) / 5)+32;
	}

	public int celsiusParaFahrenheit(int valorCelsius) {
		return ((9 * valorCelsius) / 5)+32;
	}

	public double celsiusParaFahrenheit(double valorCelsius) {
		return ((9 * valorCelsius) / 5)+32;
	}

	public int celsiusParaKelvin() {
		return 40 + 273;
	}

	public int celsiusParaKelvin(int valorCelsius) {
		return valorCelsius + 273;
	}

	public double celsiusParaKelvin(double valorCelsius) {
		return valorCelsius + 273;
	}

	public int fahrenheitParaCelsius() {
		return (40 - 32) * 5 / 9;
	}

	public int fahrenheitParaCelsius(int valorFahrenheit) {
		return (valorFahrenheit - 32) * 5 / 9;
	}

	public double fahrenheitParaCelsius(double valorFahrenheit) {
		return (valorFahrenheit - 32) * 5 / 9;
	}

	public int fahrenheitParaKelvin() {
		return ((40 - 32) * 5 / 9)+273;
	}

	public int fahrenheitParaKelvin(int valorFahrenheit) {
		return ((valorFahrenheit - 32)  * 5 / 9)+ 273;
	}

	public double fahrenheitParaKelvin(double valorFahrenheit) {
		return ((valorFahrenheit - 32) * 5 / 9)+ 273;
	}

	public int kelvinParaCelsius() {
		return 40 - 273;
	}

	public int kelvinParaCelsius(int valorKelvin) {
		return valorKelvin - 273;
	}

	public double kelvinParaCelsius(double valorKelvin) {
		return valorKelvin - 273;
	}

	public int kelvinParaFahrenheit() {
		return ((40 - 273) * 9 / 5) + 32;
	}

	public int kelvinParaFahrenheit(int valorKelvin) {
		return ((valorKelvin - 273) * 9 / 5) + 32;
	}

	public double kelvinParaFahrenheit(double valorKelvin) {
		return ((valorKelvin - 273) * 9 / 5) + 32;
	}

}
